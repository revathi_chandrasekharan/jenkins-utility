package com.jenkins.rest;



import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

/**
 * RestApiClient class to execute rest apiactions
 */
public class RestApiClient {
 
private static boolean proxyEnabled = true;
  /**
   * Method to restrict creating new object for this class. 
   */
  private RestApiClient() {

  }
   
  /**
   * Method to execute rest api post method actions
   * 
   * @param url
   *          to get access
   * @param params
   *          no of parameter to attach i post method
   * @return response as string
   */
  public static String processRestRequest(String url, Map<String, String> params) {
    String response = null;
    try {
      PostMethod postMethod = new PostMethod(url);
      Set<String> keys = params.keySet();
      for (String key : keys) {
        postMethod.addParameter(key, params.get(key));
      }
      HttpClient hc = new HttpClient();

      // proxy settings controlled by properties.
      if (proxyEnabled) {
        String proxyHost = "proxy.aspiresys.com";
        int proxyPort = 3128;
       
          hc.getHostConfiguration().setProxy(proxyHost, proxyPort);
       
      }

      hc.executeMethod(postMethod);
      response = getStringFromInputStream(postMethod.getResponseBodyAsStream());
      System.out.println("Response>>>>>>>");
      System.out.println(response);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return response;
  }

  /**
   * Method to convert input stream to string
   * 
   * @param is
   *          input stream object
   * @return
   */
  private static String getStringFromInputStream(InputStream is) {
    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();
    String line;
    try {
      br = new BufferedReader(new InputStreamReader(is));
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return sb.toString();
  }

}
